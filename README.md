# Table of Contents

## Integration Factory

* [Introduction](#markdown-header-introduction)

* [Download](#markdown-header-download)

* [Get Started](#markdown-header-get-started)

* [Integrations](#markdown-header-integrations)

* [Workflow](#markdown-header-workflow)

## Technical Implementation

* [API Reference](#markdown-header-api-reference)

    + [Methods](#markdown-header-methods)

    + [Properties](#markdown-header-properties)

* [Integration Options Reference](#markdown-header-integration-options-reference)
  
    + [Methods](#markdown-header-methods_1)

    + [Properties](#markdown-header-properties_1)

* [Campaign Data Reference](#markdown-header-campaign-data-reference)

    + [Properties](#markdown-header-properties_2)

    + [Methods](#markdown-header-methods_2)

* [Modifiers](#markdown-header-modifiers)

* [Annotation](#markdown-header-annotation)

* [Debug Advise](#markdown-header-debug-advise)

* [How to Contribute?](#markdown-header-how-to-contribute)

# Introduction

Integration Factory plugin has been designed to provide a standart solution for creating and managing third-party integrations. New integration deployment process consists of creating 3 different scripts - two on site level and one campaign script 

## Output Format

Information about campaign generation experience will be sent to third party as a string in the predefined format:

`<mode>_<campaign name>=<element1>:<variant>|<element2>:<variant>`

`<mode>` refers here either to Sandbox ("MM_Sand") or Production ("MM_Prod"). Mode prefix might come in handy when you need to filter the information out that was sent during the QA

_Example_:

`MM_Prod_T33_Basket=reduced:Default|button:green`

__NOTE:__ Output format can be changed (see [Advanced](#markdown-header-advance) section)

# Download

[Development Version](https://bitbucket.org/mm-global-se/integration-factory/src/master/src/integration-factory.js)

[Minified Version](https://bitbucket.org/mm-global-se/integration-factory/src/master/src/integration-factory.min.js)

# Get Started

* Download [minified version](https://bitbucket.org/mm-global-se/integration-factory/src/master/src/integration-factory.min.js) of Integration Factory plugin

* Create site level script "plugin_IntFactory"

* Add downloaded code

* Map site-wide with order -10

* Pick up the required integration from the [Integrations](#markdown-header-integrations) section and follow the further instructions from the integration page.

    NOTE: please contact SE team if the from client requested integration isn't listed in [Integrations](#markdown-header-integrations) section

# Integrations

+ [Google Analytics](https://bitbucket.org/mm-global-se/if-ga)

+ [Google Analytics (GTM)](https://bitbucket.org/mm-global-se/if-ga-gtm)

+ [Universal Analytics](https://bitbucket.org/mm-global-se/if-ua)

+ [CoreMetrics](https://bitbucket.org/mm-global-se/if-coremetrics)

+ [AT-Internet](https://bitbucket.org/mm-global-se/if-at-internet)

+ [Adobe Analytics (Omniture, SiteCatalyst)](https://bitbucket.org/mm-global-se/if-adobeanalytics)

+ [iJento](https://bitbucket.org/mm-global-se/if-ijento)

+ [Response Tap](https://bitbucket.org/mm-global-se/if-responsetap)

+ [User Zoom](https://bitbucket.org/mm-global-se/if-userzoom)

+ [ClickTale](https://bitbucket.org/mm-global-se/if-clicktale)

+ [SessionCam](https://bitbucket.org/mm-global-se/if-sessioncam)

+ [WebTrends](https://bitbucket.org/mm-global-se/if-webtrends)

+ [CrazyEgg](https://bitbucket.org/mm-global-se/if-crazyegg)

+ [DecibelInsight](https://bitbucket.org/mm-global-se/if-decibelinsight)

+ [Foresee](https://bitbucket.org/mm-global-se/if-foresee)

+ [Webtrekk](https://bitbucket.org/mm-global-se/if-webtrekk)

+ [Qualaroo](https://bitbucket.org/mm-global-se/if-qualaroo)

+ [NetInsight](https://bitbucket.org/mm-global-se/if-netinsight)

+ [iCognesia](https://bitbucket.org/mm-global-se/if-icognesia)

+ [Kissmetrics](https://bitbucket.org/mm-global-se/if-kissmetrics)

+ [Tealium](https://bitbucket.org/mm-global-se/if-tealium)

# Workflow

![Integration Deployment Flow.png](https://bitbucket.org/repo/EqAKrx/images/303114694-Integration%20Deployment%20Flow.png)

# API Reference

Integration Factory plugin is accessible through `mmcore.IntegrationFactory`

## Methods

### register(name, options)

Register new integrations _(called from site level script)_

Parameters:

Name    | Type     |  Description 
:-----: | :------: | :-------------------------------------------------------------------------------------------: |
name    | String   | Integration Name (e.g. "Google Analytics")                                                    |
options | Object   | Integration Options (see [Integration Options](#markdown-header-integration-options) section) |

_Example_:

```javascript

mmcore.IntegrationFactory.register('Google Analytics', {
  defaults: {},
  validate: function () {},
  check: function () {},
  exec: function () {}
});

```

__initialize(name, data)__

Initialize already integrations _(called from campaign level script)_

Parameters:

Name    | Type   | Description 
:-----: | :----: | :----------------------------------------------------------------------------------: |
name    | String | Integration Name (same as for registered with - e.g. Google Analytics)               |
data    | Object | Campaign Data (see [Campaign Data](#markdown-header-campaign-data) section)          |

_Example_:

```javascript

mmcore.IntegrationFactory.initialize('Google Analytics', {
  campaign: 'DemoCampaign',
  account: 'UA-1234567-8',
  slot: 33
});

```

## Properties

__integrations__ --> {Array}

List of registered integrations - integration name as key and integration options object as value

# Integration Options Reference

Options object contains general properties and methods required for the integration

## Methods:

__exec(data)__ _(required)_

Method that should contain the main integration logic - sending data to third-party.

Parameters:

Name    | Type   | Description 
:-----: | :----: | :----------------------------------------------------------------------------------: |
data    | Object | Campaign Data (see [Campaign Data](#markdown-header-campaign-data) section) |

_Example_:

```javascript

  exec: function (data)  {
    var prodSand = data.isProduction ? 'MM_Prod' : 'MM_Sand',
        namespace = 'mm_' + data.account,
        ga = window[data.gaVariable || '_gaq'];

    ga.push(
      [namespace + '._setAccount', data.account],
      [namespace + '._setCustomVar', data.slot, prodSand, data.campaignInfo, 1],
      [namespace + '._trackEvent', prodSand, data.campaignInfo, data.campaignInfo, undefined, true]
    );

    if (typeof data.callback === 'function') data.callback.apply(this, data);
    return true;
  },

```

__validate(data)__ _(optional)_

Optional method that validates user input - the data passed during integration initialization.

Parameters:

Name    | Type   | Description 
:-----: | :----: | :----------------------------------------------------------------------------------: |
data    | Object | Campaign Data (see [Campaign Data](#markdown-header-campaign-data) section) |

_Example_:

```javascript

validate: function (data) {
  if(!data.campaign)
      return 'No campaign.';
  return true;
},

```

__check(data)__ _(optional)_

Optional method that checks third-party endpoint availability. Interval is 50ms by default, but it can be changed by defining `interval` option. If you are sure that the third-party method/variable is on the page and you don't need to wait it - skip this method

Parameters:

Name    | Type   | Description 
:-----: | :----: | :----------------------------------------------------------------------------------: |
data    | Object | Campaign Data (see [Campaign Data](#markdown-header-campaign-data) section)          |

_Example_:

```javascript

interval: 10,

check: function (data) {
  var ga = window[data.gaVariable || '_gaq'];
  return ga && ga.push;
},

```

## Properties

__defaults__ --> {Object}

Object for storing default values. They can be account names, variable names, etc.

_Example_:

```javascript

defaults: {
  account: 'UA-1234567-8',
  dimensionName: 1,
  dimensionIndex: 1
}

```

In addition, `defaults` object has two special properties that Integration Factory listen to.

They are:

-`isStopOnDocEnd` --> {Boolean} - `true` by default. Change to `false` if you need to wait for the third-party endpoint some time after the DOM is ready.

__Important:__ depracated in from 2.1.7 version

`modifiers` --> {Object} - object that contains default modifiers methods. That methods will be executed before campaign data is sent to third-party so you can modify it before (please see [Advanced](#markdown-header-advance) section for more information)  

__interval__ --> {Number}

Define custom interval (in ms) for the `check` method (50ms by default)

_Example_:

```javascript

interval: 10,

```
_Full Example of `.register` script_:

```javascript

mmcore.IntegrationFactory.register('Google Analytics', {
  defaults: {
    modifiers: {}
  },

  validate: function (data) {
    if(!data.campaign)
        return 'No campaign.';
    return true;
  },

  check: function (data) {
    var ga = window[data.gaVariable || '_gaq'];
    return ga && ga.push;
  },

  exec: function (data)  {
    var prodSand = data.isProduction ? 'MM_Prod' : 'MM_Sand',
        namespace = 'mm_' + data.account,
        ga = window[data.gaVariable || '_gaq'];

    ga.push(
      [namespace + '._setAccount', data.account],
      [namespace + '._setCustomVar', data.slot, prodSand, data.campaignInfo, 1],
      [namespace + '._trackEvent', prodSand, data.campaignInfo, data.campaignInfo, undefined, true]
    );

    if (typeof data.callback === 'function') data.callback();

    return true;
  }
});

```

# Campaign Data Reference

Campaign Data object contains integration configuration related to some particular campaign such as campaign name, campaign type, etc, as long as custom overiddings of default properties (of `defaults` object)

All properties and methods listed in Campaign Data object will be then available in the methods of Integration Options object (listed above).

There is a number of common properties that can be populated within Campaign Data object

## Properties

__campaign__ --> {String} _(required)_

Contains campaign name

__redirect__ --> {Boolean} _(optional)_

Defines whether campaign is of redirect type or not.

Property is required, but already predefined with `false` value so you don't need add this property if the campaign is of content type

Change to `true` in case of redirect campaign

__campaignInfo__ --> {String} _(optional)_

You can provide predefined campaign generation information (in client-friendly format). In this case IntegrationFactory plugin will will be operating with this info instead of getting it from the page.

## Methods

__callback(data)__

Is executed from `exec` method after the campaign data is sent to third-party.

Parameters:

Name    | Type   | Description 
:-----: | :----: | :----------------------------------------------------------------------------------: |
data    | Object | Campaign Data (see [Campaign Data](#markdown-header-campaign-data) section)          |

_Full Example of `.initialize` script_:

```javascript

mmcore.IntegrationFactory.initialize(
  'Google Analytics', {
  campaign: 'MyCampaign',
  redirect: true,
  callback: function (data) {
    console.dir(data);
  }
});

```

# Modifiers

Modifiers are custom methods that allow you to modify campaign data before it is sent to third-party.

[page-persist](https://bitbucket.org/mm-global-se/page-persist)

# Annotation

Annotated source code of Integration Factory is available. It is very easy to read and understand. Highly recommended for advanced users. Follow the instructions below to get it:

+ Clone the repository

+ Go to _/annotation_ folder and open _integration-factory.html_ in browser.

_Preview_:

![Screen Shot 2015-05-22 at 19.43.41.png](https://bitbucket.org/repo/EqAKrx/images/101904166-Screen%20Shot%202015-05-22%20at%2019.43.41.png)

# Debug Advise

Integration errors are stored in `mm_error` variable. Check it first if something goes wrong.

If you found an issue please use "Issue" section of the repository to notify us about it. 

# How to contribute?

## Preparations

* Install node.js (find out more on [https://nodejs.org/](https://nodejs.org/)).

* Install grunt-cli: `npm install -g grunt-cli` (see the [Getting started](http://gruntjs.com/getting-started) for more information).

* Fork and clone the repo.

* Run `npm install` to install all Grunt dependencies.

## Submitting pull requests

* Create a new branch, please don't work in master directly.

* Add failing tests to "spec/integration-factory-spec.js" for the change you want to make. Run `grunt test` to see the test fail.

* Fix stuff.

* Run `grunt test` to see if the test pass. Repeat until success.

* Ideally, add `Markdown` and `jsDoc` annotations to your changes - this will be then reflacted in annotated source code that can be used later for educational purpose.

* Run `grunt` to update source code annotation and update minified version to reflect your changes.

* Push to your fork and submit a pull request.

## Syntax

* Two space indents. Don't use tabs anywhere. Use `\t` if you need a tab character in a string.

* Don't go overboard with the whitespace.

* No more than [one assignment](http://benalman.com/news/2012/05/multiple-var-statements-javascript/) per `var` statement.

* Delimit strings with single-quotes `'`, not double-quotes `"`.

* __When in doubt, follow the conventions you see used in the source already.__