//      IntegrationFactory.js 2.1.8
//      2015 Maxymiser
//      vitaliy.sobur@maxymiser.com
;(function (mmcore) {
    // Current version
    var VERSION = '2.1.8',

        // Registered integrations
        integrations = {},

        // __Register__ new integration
        // `name`: integration name
        // `options`: integration options
        /**
         * Register new integration
         *
         * @param  {String} name    Integration name
         * @param  {{validate: Function, check: Function, interval: Number, defaults: Object, exec: Function}} options
         * @return {Object} Registered integration
         */
        register = function (name, options) {
          if (typeof name !== 'string' || !name) {
              return _helpers.logError(_helpers.errorMessages[0]), null;
          }

          options = options || {};

          return (integrations[name] = {
            // Define input data validation
            validate: options.validate  || function () {return true;},
            // Define when to execute integration
            check: options.check        || function () {return true;},
            // Define time inteval for check performing
            interval: options.interval  || 50,
            // Define default properties
            defaults: options.defaults  || {},
            // Define integration execution
            exec: options.exec
          });
        },

        // __Initialize__ target integration
        // `name`: integration name
        // `data`: campaign data
        /**
         * Initialize registered integration
         *
         * @param  {String} name Integration name
         * @param  {Object} data Campaign data
         * @return {void}
         */
        initialize = function (name, data) {
          var integration = integrations[name],
              defaults = integration.defaults || {},
              validationError;

          // Check if integration `name` is defined
          if (typeof name !== 'string') {
            return _helpers.logError(_helpers.errorMessages[0], name);
          }
          // Check if integration execution method is defined
          if (!integration.exec) {
            return _helpers.logError(_helpers.errorMessages[1], name);
          }

          // Check whether the campaign is redirect campaign
          if (data.redirect &&
            // Check whether current page is generation page
            _helpers.isGenerationPage(data.campaign) &&
            // Check whether there is non-default experience
            _helpers.hasNoneDefaultExperience(data)) {
              // Save target campaign experience in case of redirect campaign
              _helpers.saveCampaignExperience(data);
          } else {
            // Add target campaign info to the `data` object if doesn't exist
            data.campaignInfo = data.campaignInfo || _helpers.getCampaignInfo(data);
            // Check if target campaign info exists
            if (data.campaignInfo) {
              // populate campaign `data` object with `defaults`
              for (var prop in defaults) {
                if (defaults.hasOwnProperty(prop) && typeof data[prop] === 'undefined') {
                  data[prop] = defaults[prop];
                }
              }
              // Add production check info to the `data` object
              data.isProduction = _helpers.isProduction();
              // Validate entry data and get possible errors
              validationError   = _helpers.isValidData.call(integration, data);
              // Check if `validationError` variable contains error message
              if (typeof validationError === 'string') {
                // Log validation error
                _helpers.logError(validationError, name);
              } else {
                // Execute defined modifiers
                _helpers.executeModifiers.call(integration, data);
                // Send target campaign information to third-party
                _helpers.sendIntegrationData.call(integration, data);
              }
            }
          }
        },

        // ## Helpers
        _helpers = {
          // List of possible integration error messages
          errorMessages: [
            'Invalid integration build, name is required.',
            'Invalid integration build, exec is required.'
          ],

          // __Log errors__
          /**
           * Log errors
           *
           * @param  {String} message
           * @param  {String} integrationName
           * @return {void}
           */
          logError: function (message, integrationName) {
            mmcore.EH(
              {message: '[' + (integrationName ? integrationName : 'Integration') + '] ' + message}
            );
          },

          // _Check_ whether current page is target campaign page and has experience information
          /**
           * Check whether current page is target campaign page and has experience information
           *
           * @param  {String}  campaignName
           * @return {Object}               Experience information for current campaign
           */
          isGenerationPage: function (campaignName) {
            return mmcore.GenInfo[campaignName];
          },

          // _Check_ whether target campaign has non default experience
          /**
           * Check whether target campaign has non default experience
           * @param  {[type]}  data campaign data
           * @return {Boolean}
           */
          hasNoneDefaultExperience: function(data) {
            var experience = this.getCampaignExperience(data),
                hasNonDefaultExperience = false,
                key;

            for (key in experience) {
              if (experience.hasOwnProperty(key) && (experience[key] !== 'Default')) {
                hasNonDefaultExperience = true;
                break;
              }
            }

            return hasNonDefaultExperience;
          },

          // __Get__ target campaign __experience__
          /**
           * Get target campaign experiance
           *
           * @param  {Object} data Campaign data set during integration initialization
           * @return {Object}      Experience information for current campaign
           */
          getCampaignExperience: function (data) {
            // Get info from `mmcore.GenInfo` object
            var experience = mmcore.GenInfo[data.campaign];

            // Get campaign info from the cookie if campaign type is redirect
            if (data.redirect && !experience) {
              var cookieName = 'mm_redir_' + data.campaign;
              experience = JSON.parse(mmcore.GetCookie(cookieName, 1) || '{}');
              mmcore.SetCookie(cookieName, '', -1, 1);
            }

            return experience;
          },

          // __Save__ target campaign __experience__ to cookies
          /**
           * Save target campaign experience to cookie (redirect case)
           *
           * @param  {Object} data Campaign data
           * @return {void}
           */
          saveCampaignExperience: function (data) {
            // Get campaign name
            var campaignName = data.campaign,
            // Get campaign experience
                experience = mmcore.GenInfo[data.campaign];
            // Save campaign experince
            mmcore.SetCookie('mm_redir_' + campaignName, JSON.stringify(experience), 0, 1);
          },

            // __Get campaign info__ in client-friendly format
            // `MyCampaign_=element1:variant|element2:variant`
            /**
             * Get campaign info in client-friendly format
             *
             * @param  {Object} data Campaign data
             * @return {String}      Campaign info in client-friendly format
             */
            getCampaignInfo: function (data) {
              var experience = this.getCampaignExperience(data),
                  campaignInfo = [],
                  element;

              // Log error if target campaign isn't generated
              if (!experience) {return this.logError(data.campaign + ' is not found.');}

              // Collect campaign experience
              for (element in experience) {
                  campaignInfo.push(element + ':' + experience[element]);
              }

              // Format and return campaign info
              return data.campaign + '=' + campaignInfo.join('|');
            },

            // __Check__ whether the page in __production mode__
            /**
             * Check whether the page in production mode
             * @return {Boolean}
             */
            isProduction: function () {
              var params = ['cfgid', 'opc.enabled', 'opc.vis', 'pt.enabled', 'un'],
                  cookies = ['cfgid', 'cfgID'],
                  i = 0;

              // check for URL params
              for(; i < params.length; i ++) {
                if (mmcore.GetParam(params[i])) {
                    return false;
                }
              }

              // check for Cookies
              for(i = 0; i < cookies.length; i ++) {
                if (mmcore.GetCookie(cookies[i])) {
                    return false;
                }
              }

              return true;
            },

            // __Check__ whether provided target camapaign __data is valid__
            /**
             * Check whether provided target camapaign data is valid
             *
             * @param  {Object}  data Campaign data
             * @return {void}
             */
            isValidData: function (data) {
              return this.validate.call(this, data);
            },

            // __Execute__ defined __modifier__ methods
            /**
             * Execute modifiers defined in defaults object
             *
             * @param  {Object} data        Campaign data
             * @param  {Object} integration
             * @return {void}
             */
            executeModifiers: function (data) {
              var modifiers = this.defaults.modifiers,
                  modifier;

              if (modifiers) {
                for(modifier in modifiers) {
                    modifiers[modifier].call(this, data);
                }
              }
            },

            // __Execution__ of `exec` method defined in target integration
            /**
             * Execution of `exec` method defined in target integration
             *
             * @param  {Object} data Campaign data
             * @return {void]}
             */
            sendIntegrationData: function (data) {
              var integration = this;

              // Check if third-party end point is available on the page
              (function checker() {
                if (integration.check(data)) {
                  // Execute integration
                  integration.exec(data);
                } else {
                  // Countinue checking if `isStopOnDocEnd` equals true
                  // or `DOM` isn't loaded yet
                  setTimeout(checker, integration.interval);
                }
              })();
            }
        };

    // Integration Factory exports
    mmcore.IntegrationFactory = mmcore.IntegrationFactory || {
      VERSION: VERSION,
      integrations: integrations,
      register: register,
      initialize: initialize
    };
})(window.mmcore || {});
