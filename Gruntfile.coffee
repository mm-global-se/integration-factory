module.exports = (grunt) ->
  require('load-grunt-tasks')(grunt)
  require('time-grunt')(grunt)

  grunt.initConfig
    uglify:
      all:
        src: 'src/integration-factory.js'
        dest: 'src/integration-factory.min.js'

    shell:
      options:
        stderr: false
      docco:
        command: 'docco -o "annotation" src/integration-factory.js'

    jasmine:
      src: 'src/integration-factory.js'
      options:
        specs: 'spec/integration-factory-spec.js'
        helpers: 'spec/integration-factory-helpers.js'

    jshint:
      options:
        jshintrc: '.jshintrc'
      lib:
        src: ['src/integration-factory.js']
      test:
        src: ['spec/integration-factory-spec.js']

  grunt.registerTask 'default', ['jshint:lib', 'shell', 'uglify']
  grunt.registerTask 'test', ['jshint:lib', 'jshint:test', 'jasmine']
