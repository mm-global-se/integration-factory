describe('Integration Factory', function () {
  describe('when register new integration', function () {
    afterEach(function () {
      mmcore.IntegrationFactory.integrations = {};
    });

    it('you should get `null` if no integration name provided', function () {
      var integration = mmcore.IntegrationFactory.register();
      expect(integration).toEqual(null);
    });

    it('you should get `null` if integrations name type differs from `string`', function () {
      var integration = mmcore.IntegrationFactory.register({});
      expect(integration).toEqual(null);
    });

    it('`exec` method should be `undefined` if no options provided', function () {
      var integration = mmcore.IntegrationFactory.register('integration');
      expect(integration.exec).toEqual(undefined);
    });

    it('`validate` and `check` methods should return `true` if no options provided', function () {
      var integration = mmcore.IntegrationFactory.register('integration');
      expect(integration.validate()).toEqual(true);
      expect(integration.check()).toEqual(true);
    });

    it('`interval` should be a number whether it is provided in options or not', function () {
      var integration1 = mmcore.IntegrationFactory.register('integration1'),
          integration2 = mmcore.IntegrationFactory.register('integration2', {interval: 500});
      expect(integration1.interval).toEqual(jasmine.any(Number));
      expect(integration2.interval).toEqual(jasmine.any(Number));
    });

    it('`defaults` object should be defined and be empty if there is no such object in `options` passed to `register` function', function () {
      var integration = mmcore.IntegrationFactory.register('integration');
      expect(integration.defaults).toEqual({});
    });

    it('`defaults` object should contain all properties from `options.defaults`', function () {
      var integration = mmcore.IntegrationFactory.register('integration', {
        defaults: {
          persistCounter: 5,
          property: {}
        }
      });
      expect(integration.defaults).toEqual({persistCounter: 5, property: {}});
    });    
  });

  describe('when initialize new integration', function () {
    beforeEach(function () {
      mmcore.IntegrationFactory.integrations = {};
      mmcore.GenInfo = {};
    });

    it('campaign `data` object should include values provided in `defaults` (if they are not already defined in `data`)', function () {
      var expected = {
        campaign: 'new campaign',
        dimension: 3,
        account: 'AU-123456-12',
        campaignInfo: 'new campaign=element1:variant1',
        isProduction: true
      },

      data = {
        campaign: 'new campaign',
        dimension: 3
      };

      mmcore.GenInfo['new campaign'] = {
        element1: 'variant1'
      };

      mmcore.IntegrationFactory.register('new-integration', {
        defaults: {
          account: 'AU-123456-12'
        },
        exec: function () {}
      });

      mmcore.IntegrationFactory.initialize('new-integration', data);

      expect(data).toEqual(expected);
    });

    it('campaign `data` properties should override default values from `defaults` object', function () {
      var expected = {
        campaign: 'new campaign',
        dimension: 3,
        account: 'AU-123456-12',
        campaignInfo: 'new campaign=element1:variant1',
        isProduction: true
      },

      data = {
        campaign: 'new campaign',
        dimension: 3
      };

      mmcore.GenInfo['new campaign'] = {
        element1: 'variant1'
      };

      mmcore.IntegrationFactory.register('new-integration', {
        defaults: {
          account: 'AU-123456-12',
          dimension: 7
        },
        exec: function () {}
      });

      mmcore.IntegrationFactory.initialize('new-integration', data);

      expect(data).toEqual(expected);
    });
  });
});
